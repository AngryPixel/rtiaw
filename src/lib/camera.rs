use crate::math::{degrees_to_radian, Ray};
use crate::math::{random_in_unit_disk, Vec3};
use crate::Real;

pub struct Camera {
    origin: Vec3,
    lower_left_corner: Vec3,
    horizontal: Vec3,
    vertical: Vec3,
    u: Vec3,
    v: Vec3,
    _w: Vec3,
    lens_radius: Real,
}

impl Camera {
    pub fn new(
        look_from: Vec3,
        loot_at: Vec3,
        vup: Vec3,
        vertical_fov_deg: Real,
        aspect_ratio: Real,
        aperture: Real,
        focus_dist: Real,
    ) -> Self {
        let theta = degrees_to_radian(vertical_fov_deg);
        let h = (theta / 2.0).tan();
        let view_port_height = 2.0 * h;
        let view_port_width = aspect_ratio * view_port_height;

        let w = (&look_from - &loot_at).unit();
        let u = vup.cross(&w).unit();
        let v = w.cross(&u);

        let origin = look_from;
        let horizontal = focus_dist * view_port_width * &u;
        let vertical = focus_dist * view_port_height * &v;
        let lower_left_corner = &origin - &horizontal / 2.0 - &vertical / 2.0 - &w * focus_dist;
        return Camera {
            origin,
            horizontal,
            vertical,
            lower_left_corner,
            u,
            v,
            _w: w,
            lens_radius: aperture / 2.0,
        };
    }

    pub fn get_ray(&self, u: Real, v: Real) -> Ray {
        let rd = self.lens_radius * random_in_unit_disk();
        let offset = (&self.u * rd.x()) + (&self.v * rd.y());
        return Ray::new_with(
            &self.origin + &offset,
            &self.lower_left_corner + u * &self.horizontal + v * &self.vertical
                - &self.origin
                - &offset,
        );
    }
}
