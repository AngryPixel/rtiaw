use crate::hitrecord::*;
use crate::materials::Material;
use crate::math::Ray;
use crate::math::Vec3;
use crate::Real;
use std::rc::Rc;

pub struct Sphere {
    pub center: Vec3,
    pub radius: Real,
    pub material: Rc<dyn Material>,
}

impl Sphere {
    pub fn new(center: Vec3, radius: Real, material: Rc<dyn Material>) -> Self {
        return Sphere {
            center,
            radius,
            material,
        };
    }
}

impl Hittable for Sphere {
    fn hit(&self, ray: &Ray, t_min: Real, t_max: Real) -> Option<HitRecord> {
        let oc = &ray.origin - &self.center;
        let a = ray.direction.length_squared();
        let half_b = oc.dot(&ray.direction);
        let c = oc.length_squared() - self.radius * self.radius;
        let discriminant = (half_b * half_b) - (a * c);
        if discriminant < 0.0 {
            return None;
        }
        let sqrtd = discriminant.sqrt();
        // Find the nearest root that lies in the acceptable range
        let mut root = (-half_b - sqrtd) / a;
        if root < t_min || t_max < root {
            root = (-half_b + sqrtd) / a;
            if root < t_min || t_max < root {
                return None;
            }
        }
        let point = ray.point_at(root);
        let outward_normal = (&point - &self.center) / self.radius;
        let record = HitRecord::new_with_face_normal(
            point,
            root,
            ray,
            outward_normal,
            self.material.as_ref(),
        );
        return Some(record);
    }
}

/// Collection/Grouping of hittable objects
pub struct HittableList {
    objects: Vec<Rc<dyn Hittable>>, // TODO: Using Rc for now, to be improved in the future?
}

impl HittableList {
    pub fn new() -> Self {
        HittableList {
            objects: Vec::with_capacity(8),
        }
    }
    pub fn add(&mut self, object: Rc<dyn Hittable>) {
        self.objects.push(object);
    }
    pub fn clear(&mut self) {
        self.objects.clear();
    }
}

impl Hittable for HittableList {
    fn hit(&self, ray: &Ray, t_min: Real, t_max: Real) -> Option<HitRecord> {
        let mut result: Option<HitRecord> = None;
        let mut closest_so_far = t_max;

        for object in &self.objects {
            match object.hit(ray, t_min, closest_so_far) {
                Some(hit) => {
                    closest_so_far = hit.t;
                    result = Some(hit);
                }
                _ => continue,
            };
        }
        return result;
    }
}
