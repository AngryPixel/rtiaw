use crate::materials::Material;
use crate::math::*;
use crate::Real;

/// Result of a Ray intersection with an object in the scene
pub struct HitRecord<'a> {
    pub point: Vec3,
    pub normal: Vec3,
    pub t: Real,
    pub material: &'a dyn Material,
    pub front_face: bool,
}

impl<'a> HitRecord<'a> {
    pub fn new_with_face_normal(
        point: Vec3,
        t: Real,
        ray: &Ray,
        outward_normal: Vec3,
        material: &'a dyn Material,
    ) -> Self {
        let front_face = ray.direction.dot(&outward_normal) < 0.0;
        let normal = if front_face {
            outward_normal
        } else {
            -&outward_normal
        };
        return HitRecord {
            point,
            normal,
            t,
            front_face,
            material,
        };
    }
}

/// Any hittable object type needs to implement this function
pub trait Hittable {
    fn hit(&self, ray: &Ray, t_min: Real, t_max: Real) -> Option<HitRecord>;
}
