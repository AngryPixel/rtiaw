#[macro_use]
mod macros;

pub type Real = f64;
pub mod camera;
pub mod geometry;
pub mod hitrecord;
pub mod materials;
pub mod math;
pub mod ppm;
