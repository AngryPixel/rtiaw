#[doc(hidden)]
macro_rules!_impl_expand_vec_operator {
    (+, $($t:tt)+) => (_impl_vec_operator!(Add, add,+, $($t)+););
    (-, $($t:tt)+) => (_impl_vec_operator!(Sub, sub,-, $($t)+););
    (*, $($t:tt)+) => (_impl_vec_operator!(Mul, mul,*, $($t)+););
    (/, $($t:tt)+) => (_impl_vec_operator!(Div, div,/, $($t)+););
}

#[doc(hidden)]
macro_rules! _impl_vec_operator {
    ($trait:ident, $trait_fn:ident, $op:tt, $type:ident, $($comp:expr),+ $(,)?) => (
        impl std::ops::$trait<$type> for $type {
            type Output = $type;
            fn $trait_fn (self, rhs:$type) -> Self::Output {
                $type {
                    e:[
                        $(self.e[$comp] $op rhs.e[$comp],)+
                    ]
                }
            }
        }
        impl std::ops::$trait<$type> for &$type {
            type Output = $type;
            fn $trait_fn (self, rhs:$type) -> Self::Output {
                $type {
                    e:[
                        $(self.e[$comp] $op rhs.e[$comp],)+
                    ]
                }
            }
        }
        impl std::ops::$trait<&$type> for $type {
            type Output = $type;
            fn $trait_fn (self, rhs:&$type) -> Self::Output {
                $type {
                    e:[
                        $(self.e[$comp] $op rhs.e[$comp],)+
                    ]
                }
            }
        }
        impl std::ops::$trait<&$type> for &$type {
            type Output = $type;
            fn $trait_fn (self, rhs:&$type) -> Self::Output {
                $type {
                    e:[
                        $(self.e[$comp] $op rhs.e[$comp],)+
                    ]
                }
            }
        }
    )
}

#[doc(hidden)]
macro_rules!_impl_expand_vec_operator_assign {
    (+, $($t:tt)+) => (_impl_vec_operator_assign!(AddAssign, add_assign,+=, $($t)+););
    (-, $($t:tt)+) => (_impl_vec_operator_assign!(SubAssign, sub_assign,-=, $($t)+););
    (*, $($t:tt)+) => (_impl_vec_operator_assign!(MulAssign, mul_assign,*=, $($t)+););
    (/, $($t:tt)+) => (_impl_vec_operator_assign!(DivAssign, div_assign,/=, $($t)+););
}

#[doc(hidden)]
macro_rules! _impl_vec_operator_assign {
    ($trait:ident, $trait_fn:ident, $op:tt, $type:ident, $($comp:expr),+ $(,)?) => (
        impl std::ops::$trait<$type> for $type {
            fn $trait_fn (&mut self, rhs:$type) {
                $(self.e[$comp] $op rhs.e[$comp];)+
            }
        }
        impl std::ops::$trait<&$type> for $type {
            fn $trait_fn (&mut self, rhs:&$type) {
                $(self.e[$comp] $op rhs.e[$comp];)+
            }
        }
    )
 }
/// Generate operator overloading implementations for basic math operations with T-T, T-&T, &T-T and
/// &T-&T
macro_rules! impl_vec_ops {
    ($type: ident, $($comp: expr),+ $(,)?) => (
        _impl_expand_vec_operator!(+,$type, $($comp,)+);
        _impl_expand_vec_operator!(-,$type, $($comp,)+);
        _impl_expand_vec_operator!(*,$type, $($comp,)+);
        _impl_expand_vec_operator!(/,$type, $($comp,)+);
        _impl_expand_vec_operator_assign!(+,$type, $($comp,)+);
        _impl_expand_vec_operator_assign!(-,$type, $($comp,)+);
        _impl_expand_vec_operator_assign!(*,$type, $($comp,)+);
        _impl_expand_vec_operator_assign!(/,$type, $($comp,)+);
    );
}
