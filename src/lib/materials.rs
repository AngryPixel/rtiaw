use crate::hitrecord::*;
use crate::math::*;
use crate::Real;

pub trait Material {
    /// Return a scattered ray with an attenuation or None if the ray was absorbed
    fn scatter(&self, ray: &Ray, rec: &HitRecord) -> Option<(Ray, Vec3)>;
}

pub struct LambertianMaterial {
    pub albedo: Vec3,
}

impl Material for LambertianMaterial {
    fn scatter(&self, _: &Ray, rec: &HitRecord) -> Option<(Ray, Vec3)> {
        let mut scatter_direction = &rec.normal + random_unit_vector();
        // Catch generated scatter direction
        if scatter_direction.near_zero() {
            scatter_direction = rec.normal.clone();
        }
        let scattered = Ray::new_with(rec.point.clone(), scatter_direction);
        Some((scattered, self.albedo.clone()))
    }
}

pub struct MetalMaterial {
    pub albedo: Vec3,
    fuzz: Real,
}

impl MetalMaterial {
    pub fn new(albedo: Vec3, fuzz: Real) -> Self {
        MetalMaterial {
            albedo,
            fuzz: if fuzz < 1.0 { fuzz } else { 1.0 },
        }
    }
}

impl Material for MetalMaterial {
    fn scatter(&self, ray: &Ray, rec: &HitRecord) -> Option<(Ray, Vec3)> {
        let reflected = ray.direction.unit().reflect(&rec.normal);
        let scattered = Ray::new_with(
            rec.point.clone(),
            reflected + self.fuzz * random_in_unit_sphere(),
        );
        if scattered.direction.dot(&rec.normal) > 0.0 {
            return Some((scattered, self.albedo.clone()));
        }
        None
    }
}

pub struct DielectricMaterial {
    pub refraction_index: Real,
}

impl DielectricMaterial {
    fn reflectance(cosine: Real, ref_idx: Real) -> Real {
        // Use Schlick's approximation for reflectance
        let mut r0 = (1.0 - ref_idx) / (1.0 + ref_idx);
        r0 = r0 * r0;
        return r0 + (1.0 - r0) * ((1.0 - cosine).powf(5.0));
    }
}

impl Material for DielectricMaterial {
    fn scatter(&self, ray: &Ray, rec: &HitRecord) -> Option<(Ray, Vec3)> {
        let attenuation = Vec3::new(1.0, 1.0, 1.0);
        let refraction_ratio = if rec.front_face {
            1.0 / self.refraction_index
        } else {
            self.refraction_index
        };

        let unit_direction = ray.direction.unit();
        let cos_theta = (-&unit_direction).dot(&rec.normal).min(1.0);
        let sin_theta = (1.0 - cos_theta * cos_theta).sqrt();

        let cannot_refract = refraction_ratio * sin_theta > 1.0;
        let direction = if cannot_refract
            || DielectricMaterial::reflectance(cos_theta, refraction_ratio) > random_real()
        {
            unit_direction.reflect(&rec.normal)
        } else {
            refract(&unit_direction, &rec.normal, refraction_ratio)
        };
        Some((Ray::new_with(rec.point.clone(), direction), attenuation))
    }
}
