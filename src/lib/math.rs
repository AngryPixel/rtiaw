/// Math primitives
use crate::Real;
use rand;
use rand::Rng;
use std::ops;

pub const PI: Real = 3.1415926535897932385;

#[inline(always)]
pub fn degrees_to_radian(degrees: Real) -> Real {
    return degrees * PI / 180.0;
}

pub fn random_real() -> Real {
    //TODO: Cache generator for better performance
    return rand::random::<Real>();
}

pub fn random_real_min_max(min: Real, max: Real) -> Real {
    return min + (max - min) * rand::random::<Real>();
}

pub fn random_in_unit_sphere() -> Vec3 {
    let mut rng = rand::thread_rng();
    loop {
        let p = Vec3::new(
            rng.gen_range(-1.0..=1.0),
            rng.gen_range(-1.0..=1.0),
            rng.gen_range(-1.0..=1.0),
        );
        if p.length_squared() >= 1.0 {
            continue;
        }
        return p;
    }
}

pub fn random_in_unit_disk() -> Vec3 {
    let mut rng = rand::thread_rng();
    loop {
        let p = Vec3::new(rng.gen_range(-1.0..=1.0), rng.gen_range(-1.0..=1.0), 0.0);
        if p.length_squared() >= 1.0 {
            continue;
        }
        return p;
    }
}

#[inline]
pub fn random_unit_vector() -> Vec3 {
    return random_in_unit_sphere().unit();
}

pub fn refract(uv: &Vec3, n: &Vec3, etai_over_etat: Real) -> Vec3 {
    //etai_over_etat = refraction ratio
    let cos_theta = (-uv).dot(n).min(1.0);
    let r_out_prep = etai_over_etat * (uv + cos_theta * n);
    let r_out_parallel = -((1.0 - r_out_prep.length_squared()).sqrt()) * n;
    return r_out_prep + r_out_parallel;
}

// --- Vec3 --------------------------------------------------------------------------------------
#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Vec3 {
    e: [Real; 3],
}

impl Vec3 {
    pub fn zero() -> Vec3 {
        Vec3 { e: [0.0_f64; 3] }
    }

    pub fn new(x: Real, y: Real, z: Real) -> Vec3 {
        Vec3 { e: [x, y, z] }
    }

    pub fn random() -> Self {
        let mut rng = rand::thread_rng();
        return Vec3 {
            e: [rng.gen(), rng.gen(), rng.gen()],
        };
    }

    pub fn random_min_max(min: Real, max: Real) -> Self {
        let mut rng = rand::thread_rng();
        return Vec3 {
            e: [
                rng.gen_range(min..=max),
                rng.gen_range(min..=max),
                rng.gen_range(min..=max),
            ],
        };
    }

    #[inline(always)]
    pub fn x(&self) -> Real {
        return self.e[0];
    }
    #[inline(always)]
    pub fn y(&self) -> Real {
        return self.e[1];
    }
    #[inline(always)]
    pub fn z(&self) -> Real {
        return self.e[2];
    }

    pub fn length(&self) -> Real {
        return self.length_squared().sqrt();
    }

    pub fn length_squared(&self) -> Real {
        return self.e[0] * self.e[0] + self.e[1] * self.e[1] + self.e[2] * self.e[2];
    }

    pub fn cross(&self, rhs: &Self) -> Self {
        Vec3 {
            e: [
                self.e[1] * rhs.e[2] - self.e[2] * rhs.e[1],
                self.e[2] * rhs.e[0] - self.e[0] * rhs.e[2],
                self.e[0] * rhs.e[1] - self.e[1] * rhs.e[0],
            ],
        }
    }
    pub fn dot(&self, rhs: &Self) -> Real {
        return self.e[0] * rhs.e[0] + self.e[1] * rhs.e[1] + self.e[2] * rhs.e[2];
    }

    pub fn unit(&self) -> Self {
        let length = self.length();
        return self / length;
    }

    pub fn near_zero(&self) -> bool {
        const S: Real = 1e-8;
        return self.e[0].abs() < S && self.e[1].abs() < S && self.e[2].abs() < S;
    }

    pub fn reflect(&self, rhs: &Vec3) -> Self {
        return self - 2.0 * self.dot(rhs) * rhs;
    }
}

//impl_vec_operator!(Vec3, Vec3, Add, add, +, 0, 1 ,2);
impl_vec_ops!(Vec3, 0, 1, 2);

impl ops::Mul<Real> for &Vec3 {
    type Output = Vec3;
    fn mul(self, rhs: Real) -> Vec3 {
        Vec3 {
            e: [self.e[0] * rhs, self.e[1] * rhs, self.e[2] * rhs],
        }
    }
}

impl ops::Mul<Real> for Vec3 {
    type Output = Vec3;
    fn mul(self, rhs: Real) -> Vec3 {
        Vec3 {
            e: [self.e[0] * rhs, self.e[1] * rhs, self.e[2] * rhs],
        }
    }
}

impl ops::Mul<&Vec3> for Real {
    type Output = Vec3;
    fn mul(self, rhs: &Vec3) -> Self::Output {
        return rhs * self;
    }
}

impl ops::Div<Real> for &Vec3 {
    type Output = Vec3;
    fn div(self, rhs: Real) -> Self::Output {
        Vec3 {
            e: [self.e[0] / rhs, self.e[1] / rhs, self.e[2] / rhs],
        }
    }
}

impl ops::Div<Real> for Vec3 {
    type Output = Vec3;
    fn div(self, rhs: Real) -> Self::Output {
        Vec3 {
            e: [self.e[0] / rhs, self.e[1] / rhs, self.e[2] / rhs],
        }
    }
}

impl ops::Mul<Vec3> for Real {
    type Output = Vec3;
    fn mul(self, rhs: Vec3) -> Self::Output {
        return &rhs * self;
    }
}

impl ops::MulAssign<Real> for Vec3 {
    fn mul_assign(&mut self, rhs: Real) {
        self.e[0] *= rhs;
        self.e[1] *= rhs;
        self.e[2] *= rhs;
    }
}

impl ops::DivAssign<Real> for Vec3 {
    fn div_assign(&mut self, rhs: Real) {
        self.e[0] /= rhs;
        self.e[1] /= rhs;
        self.e[2] /= rhs;
    }
}

impl ops::Neg for &Vec3 {
    type Output = Vec3;
    fn neg(self) -> Self::Output {
        Vec3 {
            e: [-self.e[0], -self.e[1], -self.e[2]],
        }
    }
}

impl ops::Index<usize> for Vec3 {
    type Output = Real;
    fn index(&self, i: usize) -> &Real {
        assert!(i < 3);
        return &self.e[i];
    }
}

impl ops::IndexMut<usize> for Vec3 {
    fn index_mut(&mut self, i: usize) -> &mut Real {
        assert!(i < 3);
        return &mut self.e[i];
    }
}

// --- Color -------------------------------------------------------------------------------------
pub fn write_color(
    writer: &mut impl std::io::Write,
    color: &Vec3,
    samples_per_pixel: i32,
) -> std::io::Result<()> {
    let scale = 1.0 / samples_per_pixel as Real;
    let color_scaled = Vec3::new(
        (color.x() * scale).sqrt(),
        (color.y() * scale).sqrt(),
        (color.z() * scale).sqrt(),
    );
    return writer.write_all(
        format!(
            "{} {} {}\n",
            (255.0 as Real * color_scaled.x().clamp(0.0, 0.999)) as u8,
            (255.0 as Real * color_scaled.y().clamp(0.0, 0.999)) as u8,
            (255.0 as Real * color_scaled.z().clamp(0.0, 0.999)) as u8
        )
        .as_bytes(),
    );
}

// --- Ray ----------------------------------------------------------------------------------------

pub struct Ray {
    pub origin: Vec3,
    pub direction: Vec3,
}

impl Ray {
    pub fn new() -> Self {
        Ray {
            origin: Vec3::zero(),
            direction: Vec3::zero(),
        }
    }

    pub fn new_with(origin: Vec3, direction: Vec3) -> Self {
        Ray { origin, direction }
    }

    pub fn point_at(&self, t: Real) -> Vec3 {
        let offset = &self.direction * t;
        return &offset + &self.origin;
    }
}

#[cfg(test)]
mod test {
    use super::Vec3;

    #[test]
    fn test_vec3_mul() {
        assert_eq!(
            Vec3::new(2.0, 4.0, 8.0) * Vec3::new(8.0, 4.0, 2.0),
            Vec3::new(16.0, 16.0, 16.0)
        );
    }

    #[test]
    fn test_vec3_mul_scalar() {
        assert_eq!(
            Vec3::new(2.0, 4.0, 8.0) * 2.0_f64,
            Vec3::new(4.0, 8.0, 16.0)
        );
    }

    #[test]
    fn test_vec3_mul_assign() {
        let mut v = Vec3::new(2.0, 4.0, 8.0);
        v *= Vec3::new(8.0, 4.0, 2.0);
        assert_eq!(v, Vec3::new(16.0, 16.0, 16.0));
    }

    #[test]
    fn test_vec3_div() {
        assert_eq!(
            Vec3::new(16.0, 16.0, 16.0) / Vec3::new(8.0, 4.0, 2.0),
            Vec3::new(2.0, 4.0, 8.0)
        );
    }

    #[test]
    fn test_vec3_div_scalar() {
        assert_eq!(Vec3::new(2.0, 4.0, 8.0) / 2.0_f64, Vec3::new(1.0, 2.0, 4.0));
    }

    #[test]
    fn test_vec3_add() {
        assert_eq!(
            Vec3::new(2.0, 4.0, 9.0) + Vec3::new(8.0, 4.0, 2.0),
            Vec3::new(10.0, 8.0, 11.0)
        );
    }

    #[test]
    fn test_vec3_add_assign() {
        let mut v = Vec3::new(2.0, 4.0, 8.0);
        v += Vec3::new(8.0, 4.0, 2.0);
        assert_eq!(v, Vec3::new(10.0, 8.0, 10.0));
    }
}
