use crate::math::Vec3;
use crate::Real;
/// Write out PPM compatible image file
use std::io::Write;

#[derive(Debug)]
pub struct PPMImage<'a> {
    width: usize,
    height: usize,
    bytes: &'a [Real],
}

const PPM_COLOR_CHANNELS: u32 = 3;
impl<'a> PPMImage<'a> {
    pub fn new(width: usize, height: usize, bytes: &'a [Real]) -> Result<PPMImage, ()> {
        let byte_size = width * height * (PPM_COLOR_CHANNELS as usize);
        if bytes.len() != byte_size {
            return Err(());
        }
        return Ok(PPMImage {
            width,
            height,
            bytes,
        });
    }

    pub fn write(&self, writer: &mut impl Write, samples_per_pixel: i32) -> std::io::Result<()> {
        let header = format!("P3\n{} {}\n255\n", self.width, self.height);
        writer.write_all(header.as_bytes())?;
        for j in 0..self.height {
            let index_j = self.height - j - 1;
            for i in 0..self.width {
                let offset = index_j * self.width * 3 + i * 3;
                let color = Vec3::new(
                    self.bytes[offset],
                    self.bytes[offset + 1],
                    self.bytes[offset + 2],
                );
                crate::math::write_color(writer, &color, samples_per_pixel)?;
            }
        }

        return Ok(());
    }
}
