use rtiaw::camera::Camera;
use rtiaw::geometry::*;
use rtiaw::hitrecord::*;
use rtiaw::materials::*;
use rtiaw::math::*;
use rtiaw::ppm::PPMImage;
use rtiaw::Real;
use std::fs::File;
use std::rc::Rc;

// Constants
const IMAGE_ASPECT_RATIO: Real = 3.0 / 2.0;
const IMAGE_WIDTH: usize = 1200;
const IMAGE_HEIGHT: usize = ((IMAGE_WIDTH as Real) / IMAGE_ASPECT_RATIO) as usize;
const SAMPLES_PER_PIXEL: i32 = 500;
const MAX_RAY_DEPTH: i32 = 50;

fn ray_color(ray: &Ray, world: &dyn Hittable, depth: i32) -> Vec3 {
    if depth <= 0 {
        return Vec3::zero();
    }
    if let Some(rec) = world.hit(ray, 0.001, Real::INFINITY) {
        if let Some((ray_scattered, attenuation)) = rec.material.scatter(ray, &rec) {
            return attenuation * ray_color(&ray_scattered, world, depth - 1);
        } else {
            return Vec3::zero();
        }
    }
    let unit_direction = ray.direction.unit();
    let t = 0.5 * (unit_direction.y() + 1.0);
    return &((1.0 - t) * Vec3::new(1.0, 1.0, 1.0)) + &(t * Vec3::new(0.5, 0.7, 1.0));
}

fn write_pixel(data: &mut [Real], pixel: &Vec3, i: usize, j: usize) {
    let offset = j * IMAGE_WIDTH * 3 + i * 3;
    data[offset] = pixel.x();
    data[offset + 1] = pixel.y();
    data[offset + 2] = pixel.z();
}

fn random_scene() -> HittableList {
    let mut world = HittableList::new();

    let material_ground = Rc::new(LambertianMaterial {
        albedo: Vec3::new(0.5, 0.5, 0.5),
    });
    world.add(Rc::new(Sphere::new(
        Vec3::new(0.0, -1000.0, 0.0),
        1000.0,
        material_ground.clone(),
    )));

    for a in -11..11 {
        for b in -11..11 {
            let choose_mat = random_real();
            let center = Vec3::new(
                a as Real + 0.9 * random_real(),
                0.2,
                b as Real + 0.9 * random_real(),
            );

            if (&center - Vec3::new(4.0, 0.2, 0.0)).length() > 0.9 {
                if choose_mat < 0.8 {
                    // diffuse
                    let albedo = Vec3::random() * Vec3::random();
                    let material = Rc::new(LambertianMaterial { albedo });
                    world.add(Rc::new(Sphere::new(center, 0.2, material)));
                } else if choose_mat < 0.95 {
                    // metal
                    let albedo = Vec3::random_min_max(0.5, 1.0);
                    let fuzz = random_real_min_max(0.0, 0.5);
                    let material = Rc::new(MetalMaterial::new(albedo, fuzz));
                    world.add(Rc::new(Sphere::new(center, 0.2, material)));
                } else {
                    // glass
                    let material = Rc::new(DielectricMaterial {
                        refraction_index: 1.5,
                    });
                    world.add(Rc::new(Sphere::new(center, 0.2, material)));
                }
            }
        }
    }

    let material1 = Rc::new(DielectricMaterial {
        refraction_index: 1.5,
    });
    world.add(Rc::new(Sphere::new(
        Vec3::new(0.0, 1.0, 0.0),
        1.0,
        material1,
    )));

    let material2 = Rc::new(LambertianMaterial {
        albedo: Vec3::new(0.4, 0.2, 0.1),
    });
    world.add(Rc::new(Sphere::new(
        Vec3::new(-4.0, 1.0, 0.0),
        1.0,
        material2,
    )));

    let material3 = Rc::new(MetalMaterial::new(Vec3::new(0.7, 0.6, 0.5), 0.0));
    world.add(Rc::new(Sphere::new(
        Vec3::new(4.0, 1.0, 0.0),
        1.0,
        material3,
    )));

    world
}

fn main() {
    const DATA_SIZE: usize = IMAGE_HEIGHT * IMAGE_WIDTH * 3;
    let mut vec_storage = vec![0.0; DATA_SIZE];
    let mut data = vec_storage.as_mut_slice();

    // World
    let world = random_scene();

    // Camera
    let look_from = Vec3::new(13.0, 2.0, 3.0);
    let look_at = Vec3::new(0.0, 0.0, 0.0);
    let vup = Vec3::new(0.0, 1.0, 0.0);
    let dist_to_focus = 10.0;
    let aperture = 0.1;

    let camera = Camera::new(
        look_from,
        look_at,
        vup,
        20.0,
        IMAGE_ASPECT_RATIO,
        aperture,
        dist_to_focus,
    );

    // generate image

    for j in 0..IMAGE_HEIGHT {
        print!("\rScanlines remaining: {}", IMAGE_HEIGHT - j);
        for i in 0..IMAGE_WIDTH {
            let mut pixel_color = Vec3::zero();
            for _ in 0..SAMPLES_PER_PIXEL {
                let u = (i as Real + random_real()) / (IMAGE_WIDTH - 1) as Real;
                let v = (j as Real + random_real()) / (IMAGE_HEIGHT - 1) as Real;
                let ray = camera.get_ray(u, v);
                pixel_color += ray_color(&ray, &world, MAX_RAY_DEPTH);
            }
            write_pixel(&mut data, &pixel_color, i, j);
        }
    }
    println!("\nDone");

    // Create and write PPM file

    let ppm = match PPMImage::new(IMAGE_WIDTH, IMAGE_HEIGHT, &data) {
        Ok(p) => p,
        Err(_) => {
            eprintln!("PPM Image dimensions do not match supplied data");
            return;
        }
    };

    let mut file = match File::create("test.ppm") {
        Ok(f) => f,
        Err(e) => {
            eprintln!("Failed to open file: {}", e);
            return;
        }
    };

    if let Err(e) = ppm.write(&mut file, SAMPLES_PER_PIXEL) {
        eprintln!("Failed to write to file: {}", e);
        return;
    }
    println!("Finished");
}
